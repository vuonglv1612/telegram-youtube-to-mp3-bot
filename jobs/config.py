import os

class Config:
    bot_token = os.getenv("BOT_TOKEN")
    music_channel_id = os.getenv("MUSIC_CHANNEL_ID")
    telegram_api = f'https://api.telegram.org/bot{bot_token}/sendAudio'