from celery import Celery

app = Celery('converter')

app.config_from_object('jobs.celeryconfig')

if __name__ == '__main__':
    app.start()