import os
import time

import requests
import youtube_dl
from bs4 import BeautifulSoup

from jobs.config import Config


def search(text):
    youtube_url = "https://www.youtube.com"
    params = {'search_query': text}
    res = requests.get(f'{youtube_url}/results', params=params)
    soup = BeautifulSoup(res.content, 'html.parser')
    find_title = soup.find_all('a', {'rel': 'spf-prefetch'})
    title = '{}'
    for tag in find_title:
        title = title.format(tag.text)
        break
    return {
        'found': find_title != [] ,
        'result':{
            'title': title
        }
    }

def download(title, video_url):
    now = time.time()
    ytdwl = {
        'outtmpl': f'{title}_{now}.%(ext)s',
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '128'
        }]
    }
    try:
        with youtube_dl.YoutubeDL(ytdwl) as ydl:
            ydl.download([video_url])
        file_name = f'{title}_{now}.mp3'
        success = True
    except youtube_dl.DownloadError:
        success = False
        title = ''
    return {
        'success': success,
        'result':{
            'file_name': f'{title}_{now}.mp3',
            'title': f'{title}'
        }
    }

def upload_channel(config: Config, file_name, title):
    files = {'audio': open(file_name,'rb')}
    data = {'chat_id': config.music_channel_id, 'title':title}
    response = requests.post(config.telegram_api, files=files, data=data)
    return response

def get_file_id(response):
    get_id = response.json()
    file_id = get_id.get('result', {}).get('audio', {}).get('file_id', "")
    return file_id


def upload_user(config: Config, chat_id, file_id, title):
    data = {'chat_id': chat_id, 'title':title, 'audio': file_id}
    response = requests.get(config.telegram_api, data=data)


def send_audio(config: Config, chat_id, video_id):
    search_result = search(video_id) #search
    found = search_result.get('found', False)
    if found is True:
        title = search_result.get('result', {}).get('title', '')
        download_audio = download(title, video_id) # download and convert
        success = download_audio.get('success')
        if success is True:
            file_name = download_audio.get('result', {}).get('file_name', '')
            response = upload_channel(config, file_name, title) #upload for channel
            file_id = get_file_id(response) # get file id from response
            user = upload_user(config, chat_id, file_id, title) #upload for chat_id
            os.remove(file_name)
            print(f"removed file {file_name}")
            return {
                "success": True,
                "video_id": video_id,
                "chat_id": chat_id,
                "title": title
            }
        else:
            return {
                "success": False
            }
    else: 
        {
            "success": False
        }
